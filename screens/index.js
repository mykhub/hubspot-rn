import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { createAppContainer } from 'react-navigation';

import { createRootNavigator } from '../router';
import { loginSuccess } from '../modules/reducers/auth';

class Container extends Component {
  async componentDidMount() {
    const { loginSuccess } = this.props;

    const token = await AsyncStorage.getItem('access_token');
    if (token) loginSuccess();
  }

  render() {
    const { loggedIn } = this.props;

    const Layout = createAppContainer(
        createRootNavigator(loggedIn)
    );

    return (<Layout/>);
  }
}

const mapStateToProps = state => ({
  loggedIn: state.authReducer.loggedIn,
});

const mapDispatchToProps = {
  loginSuccess,
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
