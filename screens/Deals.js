import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View, Button, AsyncStorage } from 'react-native';
import { Table, Row } from 'react-native-table-component';

import { getDealsRequest } from '../modules/reducers/deals';
import { logout } from "../modules/reducers/auth";

const styles = StyleSheet.create({
  container: { flex: 1, padding: 3, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 1 },
  row: { height: 40 },
  dataWrapper: { marginTop: -1 },
  logoutBtn: { width: 100 },
});

class Deals extends PureComponent {
  state = {
    tableHead: ['id', 'name', 'amount', 'close date', 'stage'],
    tableData: [],
  };

  componentDidMount() {
    const { getDealsRequest } = this.props;

    getDealsRequest();
  }

  handleLogout = () => {
    const { logout } = this.props;

    AsyncStorage.clear();
    alert('You have been logged out.');
    logout();
  };

  render() {
    const { deals } = this.props;
    const { tableHead } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView style={styles.dataWrapper}>
          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row data={tableHead} style={styles.head} textStyle={styles.text}/>
            {deals.map(deal =>
              <Row
                key={deal.dealId}
                data={Object.values(deal)}
                style={styles.row}
                textStyle={styles.text}/>)}
          </Table>
        </ScrollView>
        <Button onPress={this.handleLogout} title="Logout" style={styles.logoutBtn} color="grey"/>
      </View>
    );
  }
}

Deals.navigationOptions = {
  title: 'Deals',
};

const mapStateToProps = state => ({
  deals: state.dealsReducer.deals,
});

const mapDispatchToProps = {
  getDealsRequest,
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Deals);
