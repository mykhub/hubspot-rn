import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, Text, View, Button} from 'react-native';

import { loginRequest } from '../modules/reducers/auth';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  headerText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  loginButton: {
    marginTop: 100,
    alignSelf: 'center',
    width: 100,
  }
});

const Login = ({ loginRequest }) => (
  <View style={styles.container}>
    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
      <View style={styles.welcomeContainer}>
        <Text style={styles.headerText}>BoostUp</Text>
      </View>
      <View style={styles.loginButton}>
        <Button style={styles.loginButton} title="LOGIN" onPress={loginRequest} />
      </View>
    </ScrollView>
  </View>
);

Login.navigationOptions = {
  header: null,
};

const mapDispatchToProps = {
  loginRequest,
};

export default connect(null, mapDispatchToProps)(Login);
