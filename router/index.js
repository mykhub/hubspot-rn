import React from 'react';
import { Platform } from 'react-native';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import Login from '../screens/Login';
import Deals from '../screens/Deals';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

export const LoggedInStack = createStackNavigator({
  Deals: {
    screen: Deals,
    navigationOptions: { title: 'Deals'},
    config,
  },
});

export const LoggedOutStack = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: { title: 'Login' },
    config,
  },
});

export const createRootNavigator = (loggedIn) => {
  return createSwitchNavigator(
    {
      LoggedIn: {
        screen: LoggedInStack,
      },
      LoggedOut: {
        screen: LoggedOutStack,
      },
    },
    { initialRouteName: loggedIn ? "LoggedIn" : "LoggedOut" },
    );
};
