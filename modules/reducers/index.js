import { combineReducers } from 'redux';

import authReducer from './auth';
import dealsReducer from './deals';

const appReducer = combineReducers({
  authReducer,
  dealsReducer,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
