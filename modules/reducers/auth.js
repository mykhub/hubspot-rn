export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT = 'LOGOUT';

const initialState = {
  loggedIn: false,
  error: null,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        loggedIn: false,
        error: null,
      };
    case LOGIN_SUCCESS:
      return {
        loggedIn: true,
        error: null,
      };
    case LOGIN_FAILURE:
      return {
        loggedIn: false,
        error: state.error,
      };
    case LOGOUT:
      return {
        loggedIn: false,
        error: null,
      };
    default: return { ...state };
  }
}

export const loginRequest = () => ({ type: LOGIN_REQUEST });
export const loginSuccess = () => ({ type: LOGIN_SUCCESS });
export const loginFailure = error => ({ type: LOGIN_FAILURE, error });

export const logout = () => ({ type: LOGOUT });
