export const GET_DEALS_REQUEST = 'GET_DEALS_REQUEST';
export const GET_DEALS_SUCCESS = 'GET_DEALS_SUCCESS';
export const GET_DEALS_FAILURE = 'GET_DEALS_FAILURE';

const initialState = {
  deals: [],
  error: null
};

export default function dealsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_DEALS_REQUEST:
      return {
        deals: [],
        error: null
      };
    case GET_DEALS_SUCCESS:
      return {
        deals: action.deals,
        error: null
      };
    case GET_DEALS_FAILURE:
      return {
        deals: [],
        error: action.error
      };
    default: return { ...state };
  }
}

export const getDealsRequest = () => ({ type: GET_DEALS_REQUEST });
export const getDealsSuccess = deals => ({ type: GET_DEALS_SUCCESS, deals });
export const getDealsFailure = error => ({ type: GET_DEALS_FAILURE, error });
