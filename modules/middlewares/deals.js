import { call, put, takeEvery } from 'redux-saga/effects';

import { GET_DEALS_REQUEST, getDealsSuccess, getDealsFailure } from '../../modules/reducers/deals'
import { getDeals } from '../../api/deals';

const transformDeals = (deals) => {
  return deals.map(deal => ({
    dealId: deal.dealId,
    dealName: deal.properties.dealname.value,
    amount: deal.properties.amount.value,
    closeDate: deal.properties.closedate.value,
    dealStage: deal.properties.dealstage.value,
  }));
};

function* getDealsSaga() {
  try {
    const data = yield call(getDeals);
    const deals = transformDeals(data.deals);

    yield put(getDealsSuccess(deals));
  } catch (error) {
    yield put(getDealsFailure(error.message));
  }
}

function* watchDeals() {
  yield takeEvery(GET_DEALS_REQUEST, getDealsSaga);
}

export default watchDeals;
