import { all } from 'redux-saga/effects';

import watchAuth from './auth';
import watchDeals from './deals';

export default function* rootSaga() {
  yield all([
    watchAuth(),
    watchDeals()
  ]);
}
