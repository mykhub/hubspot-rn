import { call, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';

import { authorize, getToken } from '../../api/auth';
import { LOGIN_REQUEST, loginFailure, loginSuccess } from '../reducers/auth';

function* login() {
  try {
    const response = yield call(authorize);

    if (response.type === 'success') {
      const { code } = response.params;
      const { access_token } = yield call(getToken, code);

      AsyncStorage.setItem('access_token', access_token)
        .then(yield put(loginSuccess()));
    }
  } catch (error) {
    yield put(loginFailure(error));
  }
}

function* watchAuthGoogle() {
  yield takeEvery(LOGIN_REQUEST, login);
}

export default watchAuthGoogle;
