# **React Native Hubspot app**

#### Start react native app locally
`yarn install`

`expo start`

#### App folder structure
```
├── api               # api calls                 
├── assets            # fonts, images, etc                
├── components        # reusable components                   
├── constants         # shared variables                  
├── modules           # main redux folder inspired by ducks
│   ├── middlewares   # saga middlewares
│   └── reducers      # redux reducers, initialState, action, action creators   
├── router            # app navigation config
└── screens           # container components (app screens)
```

##### * More about [ducks](https://github.com/erikras/ducks-modular-redux).
