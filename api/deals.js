import axios from 'axios';

import { API_URL } from '../constants/Api';
import {AsyncStorage} from "react-native";

export async function getDeals() {
  try {
    const properties = ['dealname', 'dealstage', 'closedate', 'amount']
      .map(x => `properties=${x}`).join('&');

    const token = await AsyncStorage.getItem('access_token');

    return axios({
      method: 'get',
      url: `${API_URL}/deals/v1/deal/paged?&limit=25&${properties}`,
      headers: { 'Authorization':  `Bearer ${token}` },
      json: true,
    })
      .then((response) => response.data)
  } catch (e) {
    throw e;
  }
}

