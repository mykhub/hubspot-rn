import axios from 'axios';
import qs from 'query-string';
import { AuthSession } from 'expo';
import { CLIENT_ID, CLIENT_SECRET } from 'react-native-dotenv'

import { API_URL, BASE_URL } from '../constants/Api';

export async function authorize() {
  try {
    const redirectUrl = AuthSession.getRedirectUrl();

    return await AuthSession.startAsync({
      authUrl: `${BASE_URL}/oauth/authorize?scope=contacts%20social&redirect_uri=${redirectUrl}&client_id=${CLIENT_ID}`
    });
  } catch (e) {
    throw e;
  }
}

export function getToken(code) {
  try {
    const redirectUrl = AuthSession.getRedirectUrl();

    const formData = qs.stringify({
      grant_type: 'authorization_code',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      redirect_uri: redirectUrl,
      code,
    });

    return axios({
      method: 'post',
      url: `${API_URL}/oauth/v1/token`,
      data: formData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    })
      .then((response) => response.data)
      .catch((error) => console.log('error: ', error));
  } catch (e) {
    throw e;
  }
}
